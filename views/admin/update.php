<?php

use twofox\timeline\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model twofox\timeline\models\Timeline */

$this->title = Html::encode($model->title);
$this->params['subtitle'] = Module::t('twofox-timeline', 'Update Timeline');
$this->params['breadcrumbs'][] = [
    'label' => Module::t('twofox-timeline', 'Timeline'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->params['subtitle'];
?>
<div class="news-create">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1><?= $this->title ?> <small><?=$this->params['subtitle']?></small></h1>
            <div class="text-right">
                <?= Html::a('<i class="glyphicon glyphicon-list"></i>', ['index'],
                    [
                        'class' => 'btn btn-default btn-sm',
                        'title' => Module::t('twofox-timeline', 'List')
                    ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['view', 'id' => $model->id],
                    [
                        'class' => 'btn btn-default btn-sm',
                        'title' => Module::t('twofox-timeline', 'View')
                    ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                    [
                        'class' => 'btn btn-primary btn-sm',
                        'title' => Module::t('twofox-timeline', 'Create')
                    ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id],
                    [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => Module::t('twofox-timeline', 'Delete'),
                        'data-confirm' => Module::t('twofox-timeline', 'Are you sure to delete this item?'),
                        'data-method' => 'post',
                    ]); ?>
            </div>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
                'module' => $module,
            ]); ?>
        </div>
    </div>
</div>