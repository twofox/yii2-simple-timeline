<?php

use twofox\timeline\Module;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model twofox\timeline\models\Timeline */

$this->title = Html::encode($model->title);
$this->params['subtitle'] = Module::t('twofox-timeline', 'View Timeline');
$this->params['breadcrumbs'][] = [
    'label' => Module::t('twofox-timeline', 'Timeline'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->params['subtitle'];

$htmlPurifierOptions = [
        'HTML.SafeIframe' => true,
        'Attr.AllowedFrameTargets' => ['_blank', '_self', '_parent', '_top'],
        'URI.SafeIframeRegexp' =>
            '%^(https?:)?//(www.youtube.com/embed/|player.vimeo.com/video/|vk.com/video)%',
    ];
?>
<div class="news-view">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1><?= $this->title ?></h1>
            <div class="text-right">
                <?= Html::a('<i class="glyphicon glyphicon-list"></i>', ['index'],
                    [
                        'class' => 'btn btn-default btn-sm',
                        'title' => Module::t('twofox-timeline', 'List')
                    ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $model->id],
                    [
                        'class' => 'btn btn-success btn-sm',
                        'title' => Module::t('twofox-timeline', 'Update')
                    ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                    [
                        'class' => 'btn btn-primary btn-sm',
                        'title' => Module::t('twofox-timeline', 'Create')
                    ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id],
                    [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => Module::t('twofox-timeline', 'Delete'),
                        'data-confirm' => Module::t('twofox-timeline', 'Are you sure to delete this item?'),
                        'data-method' => 'post',
                    ]); ?>
            </div>
        </div>
        <div class="panel-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    [
                        'attribute' => 'annonce',
                        'format' => [
                            'html',
                            $htmlPurifierOptions
                        ]
                    ],
                    [
                        'attribute' => 'content',
                        'format' => [
                            'html',
                            $htmlPurifierOptions
                        ],
                        'contentOptions' => ['class'=>'superclass']
                    ],
                    [
                        'attribute' => 'status',
                        'value' => $model->getStatusArray()[$model->status]
                    ],
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'attribute' => 'user_id',
                        'value' => $model->{Yii::$app->controller->module->authorModel}
                            ->{Yii::$app->controller->module->authorNameField}
                    ]
                ],
            ]);
            ?>


        </div>
    </div>
</div>
