<?php

use yii\db\Migration;
use yii\db\Schema;

class m130524_201444_create_timeline_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
            'annonce' => Schema::TYPE_TEXT . ' NOT NULL DEFAULT ""',
            'content' => Schema::TYPE_TEXT . ' NOT NULL DEFAULT ""',
            'meta_title' => Schema::TYPE_STRING,
            'meta_keywords' => Schema::TYPE_STRING . '(200)',
            'meta_description' => Schema::TYPE_STRING . '(160)',
            'slug' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        // Foreign Keys
        $this->addForeignKey('FK_news_user', '{{%timeline}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

        // Indexes
        $this->createIndex('status', '{{%timeline}}', 'status');
        $this->createIndex('slug', $this->_tableName, ['slug'], true);
        $this->createIndex('created_at', '{{%timeline}}', 'created_at');
        $this->createIndex('updated_at', '{{%timeline}}', 'updated_at');
    }

    public function down()
    {
        $this->dropTable('{{%timeline}}');
    }
}
