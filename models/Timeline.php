<?php

namespace twofox\timeline\models;

use common\models\User;
use DOMDocument;
use twofox\timeline\Module;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\Expression;
use yii\helpers\FileHelper;
use creocoder\translateable\TranslateableBehavior;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $annonce
 * @property string $content
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $user_id
 *
 * @property User $user
 */
class Timeline extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $imageGetUrl;
    public $imageUploadPath;
    public $uploadTempPath;
    public $imageGetTempUrl;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->getModule('timeline')->tableName;
    }

    /**
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_NOT_ACTIVE => Module::t('twofox-timeline', 'NOT ACTIVE'),
            self::STATUS_ACTIVE => Module::t('twofox-timeline', 'ACTIVE')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['annonce'], 'string', 'max' => 1000],
            [['content'], 'string', 'max' => 30000],
            [['status'], 'in', 'range' => array_keys(self::getStatusArray())],
            [['status'], 'default', 'value' => self::STATUS_NOT_ACTIVE],
            [['created_at', 'updated_at', 'user_id'], 'safe'],
            [['title', 'slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('twofox-timeline', 'ID'),
            'title' => Module::t('twofox-timeline', 'Title'),
            'annonce' => Module::t('twofox-timeline', 'Description'),
            'content' => Module::t('twofox-timeline', 'Content'),
            'published' => Module::t('twofox-timeline','PUBLISHED'),
            'meta_title' => Module::t('twofox-timeline','META TITLE'),
            'meta_keywords' => Module::t('twofox-timeline','META KEYWORDS'),
            'meta_description' => Module::t('twofox-timeline','META KEYWORDS'),
            'status' => Module::t('twofox-timeline', 'Status'),
            'created_at' => Module::t('twofox-timeline', 'Created At'),
            'updated_at' => Module::t('twofox-timeline', 'Updated At'),
            'user_id' => Module::t('twofox-timeline', 'Author'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$app->controller->module->authorClass, ['id' => 'user_id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            if(!Yii::$app->user->id){
                return false;
            }
            $this->user_id = Yii::$app->user->id;
        }
        
        if (!$this->annonce || !$this->content) {
            return false;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            //когда узнали id обновляем данные, beforeSave вызовет $this->imageShift();
            $this->save();
        }

    }
    
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getTranslations(){
        return $this->hasMany(TimelineTranslation::className(), ['post_id' => 'id'])->where(['class' => self::className()]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression("'" . date('Y-m-d H:i:s') . "'"),
            ],            
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
                'uniqueValidator' => []
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['annonce', 'content', 'meta_title', 'meta_keywords', 'meta_description'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
            'galleryBehavior' => [
             'class' => GalleryBehavior::className(),
             'type' => self::tableName(),
             'extension' => 'jpg',
             'directory' => Yii::getAlias(Yii::$app->getModule('news')->pathToImages),
             'url' => Yii::getAlias(Yii::$app->getModule('news')->urlToImages),
             'versions' => [
                 'small' => function ($img) {
                     /** @var \Imagine\Image\ImageInterface $img */
                     return $img
                         ->copy()
                         ->thumbnail(new \Imagine\Image\Box(200, 200));
                 },
                 'medium' => function ($img) {
                     /** @var Imagine\Image\ImageInterface $img */
                     $dstSize = $img->getSize();
                     $maxWidth = 800;
                     if ($dstSize->getWidth() > $maxWidth) {
                         $dstSize = $dstSize->widen($maxWidth);
                     }
                     return $img
                         ->copy()
                         ->resize($dstSize);
                 },
             ]
         ]
        ];
    }
    
    

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors)." ";
        }
        return $result;
    }
}
