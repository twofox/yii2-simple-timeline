<?php

namespace twofox\timeline\controllers\backend;

use Yii;
use twofox\timeline\models\Timeline;
use twofox\timeline\models\TimelineSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use vova07\imperavi\actions\GetAction;
use zxbodya\yii2\galleryManager\GalleryManagerAction;

/**
 * AdminController implements the CRUD actions for Timeline model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get'],
                'view' => ['get'],
                'create' => ['get', 'post'],
                'update' => ['get', 'put', 'post'],
                'delete' => ['post', 'delete'],
                'batch-delete' => ['post', 'delete']
            ]
        ];

        return $behaviors;
    }

    public function actions()
    {
        $module = Yii::$app->getModule('news');    
            
        $actions = [
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => $this->module->imageGetTempUrl, // URL адрес папки где хранятся изображения.
                'path' => $this->module->uploadTempPath, // Или абсолютный путь к папке с изображениями.
                'type' => GetAction::TYPE_IMAGES,
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => $this->module->imageGetTempUrl, // URL адрес папки куда будут загружатся изображения.
                'path' => $this->module->uploadTempPath // Или абсолютный путь к папке куда будут загружатся изображения.
            ],
        ];
        
        
        
        if ($module->useGallery) {
            $actions['galleryApi'] = ['class' => GalleryManagerAction::className()];
            $actions['galleryApi']['types'][Yii::$app->getModule('timeline')->tableName] = Timeline::className();
        }
        
        return $actions;
    }

    /**
     * Lists all Timeline models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimelineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Timeline model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->actionUpdate(null);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer|null $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        if ($id === null) {
            $model = new Timeline;
        } else {
            $model = $this->findModel($id);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{

                foreach (Yii::$app->request->post('TimelineTranslation', []) as $language => $data) {
                    foreach ($data as $attribute => $translation)
                        $model -> translate($language) -> $attribute = $translation;
                    
                    $model -> translate($language) -> class = $model -> className();
                    $model -> translate($language) -> post_id = $model->id;
                }
                
                if($model->save()){
                    Yii::$app->session->setFlash('success', Yii::t('eugenekei-timeline', 'SAVE_SUCCESS'));
                    return $this->redirect(['update', 'id' => $model->id]);
                } else{
                    Yii::$app->session->setFlash('error', Yii::t('eugenekei-timeline', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        
        $module = Yii::$app->getModule('news');
        
        return $this->render($id === null ? 'create' : 'update', [
            'model' => $model,
            'module' => $module,
        ]);
    }

    /**
     * Deletes an existing Timeline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $path = Yii::getAlias($this->module->imageUploadPath).DIRECTORY_SEPARATOR.$id;
        FileHelper::removeDirectory($path);

        return $this->redirect(['index']);
    }
    
    /**
     * Delete multiple Timeline.
     */
    public function actionBatchDelete() {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $models = $this->findModelAll($ids);
            foreach ($models as $model) {
                $model->delete();
                $path = Yii::getAlias($this->module->imageUploadPath).DIRECTORY_SEPARATOR.$model->id;
                FileHelper::removeDirectory($path);
            }
            return $this->redirect(['index']);
        } else {
            throw new HttpException(400);
        }
    }

    /**
     * Finds the Timeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Timeline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
    
    /**
     * Finds the Timeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelAll($id)
    {
        if (($model = Timeline::find()->where(['id' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}

