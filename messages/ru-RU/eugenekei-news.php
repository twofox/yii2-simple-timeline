<?php

/**
 * Message translations for \twofox\timeline.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'ID' => 'ID',
    'Actions' => 'Действия',
    'Create' => 'Создать',
    'Update' => 'Обновить',
    'Update Timeline' => 'Обновить новость',
    'Delete' => 'Удалить',
    'View' => 'Посмотреть',
    'List' => 'К списку',
    'List Timeline' => 'Список новостей',
    'View Timeline' => 'Просмотр новости',
    'Title' => 'Заголовок',
    'Content' => 'Контент',
    'Annonce' => 'Анонс',
    'Status' => 'Статус',
    'Timeline' => 'Новости',
    'Author' => 'Автор',
    'ACTIVE' => 'Активна',
    'Read more...' => 'Читать далее...',
    'NOT ACTIVE' => 'Не активна',
    'Created At' => 'Дата публикации',
    'Updated At' => 'Последнее обновление',
    'Delete selected' => 'Удалить выделенное',
    'Are you sure to delete this item?' => 'Вы действительно хотите удалить этот элемент?',
    'Are you sure you want to delete selected items?' => 'Вы действительно хотите удалить выбранные элементы?',
    'You need to select at least one item.' => 'Вы ничего не выбрали.',

];